#ifndef MEMRAWR_GLOBAL_H
#define MEMRAWR_GLOBAL_H

#include <string>

#define BIT(index) (std::uint64_t(1) << (index))

class Hex
{
public:
    Hex(size_t num = 0) : data(num) { }
    Hex(const Hex &other) = default;

    enum AlphaCase
    {
        Lower = 0,
        Upper = 1
    };

    /// TODO: alphaCase support (not supported yet)
    std::string toString(AlphaCase alphaCase = Lower, const std::string &prefix = "0x") const;

    static Hex fromString(const std::string &text, bool *ok = nullptr);
/*
    inline bool operator ==(const Hex &other) const { return data == other.data; }
    inline bool operator !=(const Hex &other) const { return data != other.data; }
    inline bool operator  <(const Hex &other) const { return data  < other.data; }
    inline bool operator <=(const Hex &other) const { return data <= other.data; }
    inline bool operator  >(const Hex &other) const { return data  > other.data; }
    inline bool operator >=(const Hex &other) const { return data >= other.data; }

    inline Hex operator +(const Hex &other) const { return Hex(data + other.data); }
    inline Hex operator -(const Hex &other) const { return Hex(data - other.data); }
*/
    inline Hex &operator +=(const Hex &other) { data += other.data; return *this; }
    inline Hex &operator -=(const Hex &other) { data -= other.data; return *this; }

    operator size_t() const { return data; }

    size_t data;
};

#endif // MEMRAWR_GLOBAL_H
