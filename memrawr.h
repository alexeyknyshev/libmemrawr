#ifndef MEMRAWR_H
#define MEMRAWR_H

#include <string>
#include <list>

#include <global.h>

struct MemRegion
{
    MemRegion(const std::string &procMapsLine, int regionPid);
    MemRegion(const MemRegion &other) = default;
    MemRegion(MemRegion &&region) = default;

    Hex begin;
    Hex end;

    /// read / write / exec / status (shared|private)
    enum Permissions : size_t
    {
        Perm_Read   = BIT(0),
        Perm_Write  = BIT(1),
        Perm_Exec   = BIT(2),
        Perm_Shared = BIT(3),

        Perm_RW  = Perm_Read | Perm_Write,
        Perm_All = Perm_Read | Perm_Write | Perm_Exec | Perm_Shared
    };

    size_t permissions;

    Hex offset;

    size_t deviceMajor;
    size_t deviceMinor;

    /// if the region was mapped from a file,
    /// this is the file inode 0 otherwise.
    size_t inode;

    std::string path;

    /// ---------------

    const int pid;

    bool isValid() const
    {
        return (begin != end) && (begin != 0);
    }

    std::string getPermissionsText() const;

    inline size_t getSize() const
    { return end.data - begin.data; }

    enum TypeMask
    {
        Type_Unknown = 0,

        Type_Exec  = BIT(0),
        Type_Stack = BIT(1),
        Type_Heap  = BIT(2),
        Type_BSS   = BIT(3),
//        Type_SO    = BIT(4), // Shared Object .so
        Type_File  = BIT(5),

        Type_All   = Type_Exec | Type_Stack | Type_Heap | Type_BSS  | Type_File
    };

    MemRegion::TypeMask getTypeMask(std::string &outDescription) const;

    std::string getInfoString() const;

    /// TODO: seek / read / write
    // bool seekOffset(size_t offset);

    bool lock();
    bool unlock();

    inline bool isLocked() const
    {
        return mLocked;
    }

    size_t read(const Hex &offset, char *buffer, size_t readSize);
    size_t write(const Hex &offset, const char *buffer, size_t writeSize);

    /// TODO: maxSizeOf string
    char *readCString(Hex offset);

private:
    bool mLocked;
};

class MemRegionList : public std::list<MemRegion>
{
public:
    MemRegion getRegionByAddress(const Hex &address);
};

class MemRawrProcessHandle;

class MemRawr
{
public:
    MemRawrProcessHandle *attachToProcessByPid(int pid);

    /// WARNING: typeMask filtering is not implemented yet
    static MemRegionList readRegions(int pid,
                                     size_t permissionMask = MemRegion::Perm_All,
                                     size_t typeMask = MemRegion::Type_All);
};

#endif // MEMRAWR_H
