#include "memrawr.h"

#include <string>
#include <fstream>
#include <sstream>

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <sys/ptrace.h>
#include <sys/wait.h>

constexpr static size_t charsInLong = sizeof(long) / sizeof(char);

MemRegion::MemRegion(const std::string &procMapsLine, int regionPid)
    : pid(regionPid),
      mLocked(false)
{
    if (procMapsLine.empty())
    {
        begin.data = 0;
        end.data = 0;
        permissions = 0;
        offset.data = 0;
        deviceMajor = 0;
        deviceMinor = 0;
        inode = 0;
        return;
    }

    char perm[4]; /// rwx + cow bits
    char pathBuf[procMapsLine.size()];
    memset(pathBuf, '\0', procMapsLine.size());

    std::sscanf(procMapsLine.c_str(), "%lx-%lx %s %lx %lx:%lx %lx %s",
                &begin.data, &end.data, perm, &offset.data,
                &deviceMajor, &deviceMinor, &inode, pathBuf);

    path = pathBuf;

    permissions = 0;
    if (perm[0] == 'r')
    {
        permissions |= BIT(0);
    }
    if (perm[1] == 'w')
    {
        permissions |= BIT(1);
    }
    if (perm[2] == 'x')
    {
        permissions |= BIT(2);
    }
    if (perm[3] == 's')
    {
        permissions |= BIT(3);
    }
}

std::string MemRegion::getPermissionsText() const
{
    std::string result;

    if (permissions & Perm_Read)
    {
        result += 'r';
    }
    else
    {
        result += '-';
    }

    if (permissions & Perm_Write)
    {
        result += 'w';
    }
    else
    {
        result += '-';
    }

    if (permissions & Perm_Exec)
    {
        result += 'x';
    }
    else
    {
        result += '-';
    }

    /// Shared or private region
    if (permissions & Perm_Shared)
    {
        result += 's';
    }
    else
    {
        result += 'p';
    }

    return result;
}

MemRegion::TypeMask MemRegion::getTypeMask(std::string &outDescription) const
{
    if (path.empty())
    {
        outDescription = "BSS";
        return Type_BSS;
    }

    if (path == "[heap]")
    {
        outDescription = "heap";
        return Type_Heap;
    }

    static const std::string stackPattern = "[stack";
    static const std::string stackStr = "[stack]";

    if (path.find(stackPattern) == 0)
    {
        if (path == stackStr)
        {
            outDescription = "stack of main thread";
        }
        else
        {
            /// +1 in first param to skip ':' delim, -2 in second due to skiping ':' and ending ']' (2 chars)
            const std::string threadIdStr = path.substr(stackPattern.size() + 1, path.size() - stackPattern.size() - 2);
            outDescription = "stack of thread " + threadIdStr;
        }
        return Type_Stack;
    }

    struct stat st;
    if (stat(path.c_str(), &st) != -1)
    {
        const std::string command = "file -b " + path;
        FILE *pipe = popen(command.c_str(), "r");
        if (pipe)
        {
            const static size_t PATH_MAX = 4096;
            char buf[PATH_MAX];
            std::fgets(buf, PATH_MAX, pipe);
            std::fclose(pipe);

            outDescription = buf;
            if (!outDescription.empty() &&
                 outDescription.at(outDescription.size() - 1) == '\n')
            {
                outDescription.erase(outDescription.size() - 1);
            }
        }
        else
        {
            outDescription = "File";
        }
        return Type_File;
    }

    outDescription = "Unknown";
    return Type_Unknown;
}

std::string MemRegion::getInfoString() const
{
    return begin.toString() + "-" + end.toString() + " " +
           getPermissionsText() + " " + offset.toString() + " " +
           std::to_string(deviceMajor) + ":" + std::to_string(deviceMinor) + " " +
           std::to_string(inode) + " " + path;
}

/// ---------------------------------------

/*
bool MemRegion::seekOffset(size_t offset)
{

}
*/

bool MemRegion::lock()
{
    if (ptrace(PTRACE_ATTACH, pid, NULL, NULL) == -1)
    {
        return false;
    }

    int status = 0;
    if (waitpid(pid, &status, 0) == -1 || !WIFSTOPPED(status))
    {
        return false;
    }

    mLocked = true;

    return true;
}

size_t MemRegion::read(const Hex &offset, char *buffer, size_t readSize)
{
    if (!mLocked || offset < begin || offset >= end)
    {
        return 0;
    }

    if (readSize == 0)
    {
        return 0;
    }

    size_t bufferOffset = 0;

    while (true)
    {
        errno = 0;

        union
        {
            long word;
            char byte[charsInLong];
        } data;

        data.word = ptrace(PTRACE_PEEKDATA, pid, offset + bufferOffset, NULL);
        if (data.word == -1 && errno != 0)
        {
            if (errno == EIO || errno == EFAULT)
            {
                /// Error, but could be resolved
            }

            /// Error
        }

        if (readSize > charsInLong)
        {
            memcpy(&buffer[bufferOffset], &data.byte, charsInLong);
        }
        else
        {
            memcpy(&buffer[bufferOffset], &data.byte, readSize);
        }

        if (readSize <= charsInLong)
        {
            bufferOffset += readSize;
            break;
        }

        bufferOffset += charsInLong;
        readSize -= charsInLong;
    }

    return bufferOffset;
}

size_t MemRegion::write(const Hex &offset, const char *buffer, size_t writeSize)
{
    if (!mLocked || offset < begin || offset >= end)
    {
        return 0;
    }

    if (writeSize == 0)
    {
        return 0;
    }

    size_t bufferOffset = 0;

    while (true)
    {
        errno = 0;

        if (ptrace(PTRACE_POKEDATA, pid, offset + bufferOffset, *(long *)(buffer + bufferOffset)) == -1)
        {
            if (errno == EIO || errno == EFAULT)
            {
                /// Error, but could be resolved
            }
            break;
        }

        bufferOffset += charsInLong;
    }

    return bufferOffset;
}

char *MemRegion::readCString(Hex offset)
{
    if (!mLocked || offset < begin || offset >= end)
    {
        return 0;
    }

    char *result = nullptr;
    size_t resultSize = 0;

    char buffer[charsInLong];

    while (true)
    {
        /// TODO: check read size
        if (read(offset, buffer, charsInLong) == 0)
        {
            break;
        }

        if (result == nullptr)
        {
            result = (char *)malloc(1);
            result[0] = '\0';
            resultSize = 1;
        }

        for (size_t i = 0; i < charsInLong; ++i)
        {
            if (buffer[i] == '\0')
            {
                result = (char *)realloc(result, resultSize + i);
                memcpy(&result[resultSize - 1], buffer, i);
                result[resultSize + i - 1] = '\0';

                return result;
            }
        }

        result = (char *)realloc(result, resultSize + charsInLong);
        memcpy(&result[resultSize - 1], buffer, charsInLong);
        resultSize += charsInLong;
        result[resultSize - 1] = '\0';

        offset += sizeof(long);
    }

    if (result)
    {
        result = (char *)realloc(result, resultSize + 1);
        result[resultSize] = '\0';
    }

    return result;
}

bool MemRegion::unlock()
{
    return ptrace(PTRACE_DETACH, pid, 1, 0) == 0;
}

/// ---------------------------------------

MemRegion MemRegionList::getRegionByAddress(const Hex &address)
{
    for (const MemRegion &region : *this)
    {
        if (address >= region.begin &&
            address <  region.end)
        {
            return region;
        }
    }

    return MemRegion("", 0);
}

/// ---------------------------------------

MemRegionList MemRawr::readRegions(int pid, size_t permissionMask, size_t typeMask)
{
    MemRegionList regionList;

    /* check if target is valid */
    if (pid == 0)
    {
        return regionList;
    }

    const std::string mapsFilePath = "/proc/" + std::to_string(pid) + "/maps";

    std::ifstream mapsFileStream;
    mapsFileStream.open(mapsFilePath);
    if (!mapsFileStream)
    {
        /// TODO: error handler
        return regionList;
    }

    /// show_info("maps file located at %s opened.\n", name);

    std::string line;
    while (std::getline(mapsFileStream, line))
    {
        MemRegion region(line, pid);
        if ( (permissionMask == MemRegion::Perm_All) ||
             ( (region.permissions & permissionMask) == permissionMask) )
        {
            regionList.push_back(region);
        }

        /// TODO: implement typeMask
    }

    return regionList;
}
