#include <global.h>

#include <sstream>
#include <iomanip>
#include <cerrno>

std::string Hex::toString(AlphaCase alphaCase, const std::string &prefix) const
{
    std::stringstream stream;
    stream << prefix
           << std::setfill('0') << std::setw(sizeof(size_t) * 2)
           << std::hex << data;

    return stream.str();
}

Hex Hex::fromString(const std::string &text, bool *ok)
{
    errno = 0;
    const unsigned long long data = strtoull(text.c_str(), NULL, 16);
    if (errno == ERANGE && ok)
    {
        *ok = false;
    }
    else if (ok)
    {
        *ok = true;
    }

    return Hex(data);
}
