#include "memrawrprocesshandle.h"

#include <memrawr.h>

class MemRawrProcessHandlePrivate
{
public:
    MemRawrProcessHandlePrivate(MemRawr *memrawr, int pid, const std::string &name)
    {
        this->memrawr = memrawr;
        this->pid = pid;
        this->name = name;
    }

    MemRawr *memrawr;
    int pid;
    std::string name;
    MemRegionList memRegionList;
};


MemRawrProcessHandle::MemRawrProcessHandle(MemRawr *memrawr, int pid, const std::string &name)
    : d(new MemRawrProcessHandlePrivate(memrawr, pid, name))
{
    updateRegions();
}

void MemRawrProcessHandle::updateRegions()
{
    d->memRegionList = d->memrawr->readRegions(d->pid);
}

bool MemRawrProcessHandle::isAddressMapped(const Hex &address) const
{
    MemRegion region = d->memRegionList.getRegionByAddress(address);
    return region.isValid();
}
