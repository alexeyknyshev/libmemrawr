#ifndef MEMRAWRPROCESSHANDLE_H
#define MEMRAWRPROCESSHANDLE_H

#include <string>

class MemRawr;
class MemRawrProcessHandlePrivate;
class Hex;

class MemRawrProcessHandle
{
    MemRawrProcessHandle(MemRawr *memrawr, int pid, const std::string &name);
    MemRawrProcessHandle(const MemRawrProcessHandle &other) = delete;
    MemRawrProcessHandle(MemRawrProcessHandlePrivate &&other) = delete;

    inline const std::string &getName() const
    { return mName; }

    void updateRegions();

    bool isAddressMapped(const Hex &address) const;

    template<class Type>
    Type readAddress() const;

private:
    MemRawrProcessHandlePrivate *d;

    std::string mName;
};

#endif // MEMRAWRPROCESSHANDLE_H
