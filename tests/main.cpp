#include <memrawr.h>

#include <iostream>
#include <sstream>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: memrawr_test pid" << std::endl;
        return 1;
    }

    MemRawr mrawr;

    std::istringstream stream(argv[1]);
    int pid = 0;
    stream >> pid;

    if (!stream)
    {
        std::cerr << "Couldn't convert pid argument \"" << argv[1] <<
                     "\" to int." << std::endl;
        return 2;
    }

     MemRegionList regionList = mrawr.readRegions(pid, MemRegion::Perm_RW);
     for (const MemRegion &region : regionList)
     {
         std::string description;
         if ((region.permissions & MemRegion::Perm_RW) &&
             (region.getTypeMask(description) &
                 (MemRegion::Type_All & ~(MemRegion::Type_File | MemRegion::Type_Unknown))))
         {
                     std::cout << region.getInfoString() << "\t" << description << std::endl;
         }
     }
     std::cout << "Total regions count: " << regionList.size() << std::endl;

     return 0;
}
